﻿namespace CacheInvalidation.EF
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public class CacheInvalidationConfigurationException : Exception
    {
        public CacheInvalidationConfigurationException()
        {
        }

        public CacheInvalidationConfigurationException(string message) : base(message)
        {
        }

        public CacheInvalidationConfigurationException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected CacheInvalidationConfigurationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}