namespace CacheInvalidation.EF
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Entity;
    using System.Data.SqlClient;

    public class SqlInvalidationStorage : IInvalidationStorage
    {
        private Func<SqlConnection> GetSqlConnection;
        private Func<SqlTransaction> GetSqlTransaction;

        public SqlInvalidationStorage(Func<SqlConnection> getSqlConnection, Func<SqlTransaction> getSqlTransaction)
        {
            GetSqlConnection = getSqlConnection;
            GetSqlTransaction = getSqlTransaction;
        }

        public static SqlInvalidationStorage FromDbContext(DbContext db)
        {
            return new SqlInvalidationStorage(
                () => (SqlConnection)db.Database.Connection,
                () => (SqlTransaction)db.Database.CurrentTransaction?.UnderlyingTransaction);
        }

        public void UpdateSet(string setType)
        {
            string sql = string.Format(SQL_UpdateSet, setType);
            var sqlConnection = GetSqlConnection();
            OpenIfClosed(sqlConnection);
            using (SqlCommand cmd = new SqlCommand(sql, sqlConnection))
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                cmd.Transaction = GetSqlTransaction();
                cmd.Parameters.Add("@guid", SqlDbType.UniqueIdentifier).Value = Guid.NewGuid();
                cmd.ExecuteNonQuery();
            }
        }


        private const string SQL_UpdateSet = @"update [{0}_InvalidationOfSet] set [timestamp] = @guid";

        public void UpdateEntity(string setType, IEnumerable<object> idEnities)
        {
            if (idEnities == null)
                throw new ArgumentNullException(nameof(idEnities));



            var sqlConnection = GetSqlConnection();
            OpenIfClosed(sqlConnection);

            throw new NotImplementedException();


        }

        public void UpdateEntity(string setType, object idEnity)
        {
            string sql = string.Format(SQL_UpdateEntity, setType);
            var sqlConnection = GetSqlConnection();
            using (SqlCommand cmd = new SqlCommand(sql, sqlConnection))
            {
                OpenIfClosed(sqlConnection);
                cmd.Transaction = GetSqlTransaction();
                cmd.Parameters.Add(AsIdParameter(idEnity));
                cmd.Parameters.Add("@guid", SqlDbType.UniqueIdentifier).Value = Guid.NewGuid();
                cmd.ExecuteNonQuery();
            }
        }

        static SqlParameter AsIdParameter(object value)
        {
            if (value == null)
                throw new CacheInvalidationConfigurationException("Primary key is null");

            SqlDbType t;
            if (value is Guid? || value is Guid) t = SqlDbType.UniqueIdentifier;
            else if (value is long || value is long?) t = SqlDbType.BigInt;
            else if (value is int || value is int?) t = SqlDbType.Int;
            else if (value is short || value is short?) t = SqlDbType.SmallInt;
            else if (value is string) t = SqlDbType.NVarChar;
            else if (value is byte[]) t = SqlDbType.Binary;
            else
                throw new CacheInvalidationConfigurationException("Primary key of type " + value.GetType() + " is not supported");

            SqlParameter ret = new SqlParameter("@id", t);
            ret.Value = value;
            return ret;
        }

        private const string SQL_UpdateEntity = @"
if exists(select top 1 1 from [{0}_InvalidationOfEntities] Where Id=@Id) 
 Update [{0}_InvalidationOfEntities] Set [timestamp]=@guid Where Id=@Id
else
 Insert [{0}_InvalidationOfEntities](Id,[timestamp]) Values(@Id,@guid)
";

        public bool CheckSet(string setType, out byte[] timestamp)
        {
            string sql = string.Format(SQL_CheckSet, setType);
            var sqlConnection = GetSqlConnection();
            using (SqlCommand cmd = new SqlCommand(sql, sqlConnection))
            {
                OpenIfClosed(sqlConnection);
                cmd.Transaction = GetSqlTransaction();
                object raw = cmd.ExecuteScalar();
                if (raw != null)
                {
                    timestamp = ((Guid) raw).ToByteArray();
                    return true;
                }
            }

            timestamp = null;
            return false;
        }

        private const string SQL_CheckSet = @"select top 1 [timestamp] from [{0}_InvalidationOfSet]";

        public bool CheckEntity(string setType, object id, out byte[] timestamp)
        {
            string sql = string.Format(SQL_CheckEntity, setType);
            var sqlConnection = GetSqlConnection();
            using (SqlCommand cmd = new SqlCommand(sql, sqlConnection))
            {
                OpenIfClosed(sqlConnection);
                cmd.Transaction = GetSqlTransaction();
                cmd.Parameters.Add(AsIdParameter(id));
                object raw = cmd.ExecuteScalar();
                if (raw != null)
                {
                    timestamp = ((Guid)raw).ToByteArray();
                    return true;
                }
            }

            timestamp = null;
            return false;
        }

        private const string SQL_CheckEntity = @"
Select [timestamp] from [{0}_InvalidationOfEntities] Where Id=@Id";

        private static void OpenIfClosed(SqlConnection sqlConnection)
        {
            if (sqlConnection.State == ConnectionState.Closed)
                sqlConnection.Open();
        }

    }
}