﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CacheInvalidation.EF
{
    using System.Data.Common;

    public interface IInvalidationStorage
    {
        void UpdateSet(string setType);
        void UpdateEntity(string setType, object idEnity);

        bool CheckSet(string setType, out byte[] timestamp);
        bool CheckEntity(string setType, object idEntity, out byte[] timestamp);
    }

    public interface IInvalidationStorageBuilder
    {
        void PrepareSet(string setType);
        void PrepareEntity(string setType, ColumnMeta keyMeta);
    }

    public class ColumnMeta
    {
        public SupportedKeyTypes Type { get; set; }
/*
        public int Size { get; set; }
        public int Precision { get; set; }
*/
    }

    /// <summary>
    /// EDM Types, supported by CacheInvalidator as a PK types
    /// </summary>
    public enum SupportedKeyTypes
    {
        Int16,
        Int32,
        Int64,
        Guid,
        String,
        Binary,

/*
        Boolean,
        Byte,
        DateTime,
        Decimal,
        SByte,
        Time,
        DateTimeOffset,
*/
    }


}
