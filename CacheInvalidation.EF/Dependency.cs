﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CacheInvalidation.EF
{
    public class Dependency
    {
        public virtual string EntitySet { get; set; }
        public virtual object OptionalKey { get; set; }

        protected bool Equals(Dependency other)
        {
            return string.Equals(EntitySet, other.EntitySet) && Equals(OptionalKey, other.OptionalKey);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Dependency) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((EntitySet != null ? EntitySet.GetHashCode() : 0)*397) ^ (OptionalKey != null ? OptionalKey.GetHashCode() : 0);
            }
        }
    }

    public class Dependency<TEntity> : Dependency
    {
        public static readonly Dependency EntireSet = new Dependency()
        {
            EntitySet = string.Intern(typeof(TEntity).FullName)
        };
    }


    public class Dependency<TEntity, TKey> : Dependency
    {
        public Dependency(TKey key)
        {
            EntitySet = string.Intern(typeof(TEntity).FullName);
            OptionalKey = key;
        }
    }
}
