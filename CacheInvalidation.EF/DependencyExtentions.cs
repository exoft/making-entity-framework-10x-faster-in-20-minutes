﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CacheInvalidation.EF
{
    using System.Runtime.Caching;

    public static class DependencyExtentions
    {
        private static MemoryCache Cache = new MemoryCache("All");
        public static long TotalRequests = 0;
        public static long TotalHits = 0;


        public static TResult LazyLoad<TResult>(
            this Dependency dependency,
            string key,
            CacheInvalidator invalidator,
            Func<TResult> function
            )
        {
            return new[] { dependency }.LazyLoad(key, invalidator, function);
        }

        public static TResult LazyLoad<TResult>(
            this IEnumerable<Dependency> dependencies,
            string key,
            CacheInvalidator invalidator,
            Func<TResult> function
            )
        {
            Interlocked.Increment(ref TotalRequests);
            var dependenciesCopy = dependencies.ToList();
            string keySuffix = GetKeySuffix(dependenciesCopy);
            CacheHolder<TResult> cacheItem = (CacheHolder<TResult>)Cache.Get(key+keySuffix);

            // retrive all the current timestamps from DB
            List<byte[]> currentResults = new List<byte[]>();
            foreach (var dependency in dependenciesCopy)
            {
                byte[] timestamp;
                bool isOk;
                if (dependency.OptionalKey == null)
                    isOk = invalidator.InvalidationStorage.CheckSet(dependency.EntitySet, out timestamp);
                else
                    isOk = invalidator.InvalidationStorage.CheckEntity(dependency.EntitySet, dependency.OptionalKey, out timestamp);

                currentResults.Add(timestamp);
            }

            bool isEqual =
                cacheItem != null
                && cacheItem.Dependencies.Length == dependenciesCopy.Count;

            if (isEqual)
            {
                for (int d = 0; d < cacheItem.Dependencies.Length; d++)
                {
                    if (!dependenciesCopy[d].Equals(cacheItem.Dependencies[d]))
                    {
                        isEqual = false;
                        break;
                    }

                    var storedStamp = cacheItem.Timestamps[d];
                    var actualStamp = currentResults[d];
                    if (!TimestampComparer.AreSame(storedStamp, actualStamp))
                    {
                        isEqual = false;
                        break;
                    }
                }
            }

            if (isEqual)
            {
                Interlocked.Increment(ref TotalHits);
                return cacheItem.Content;
            }

            TResult content = function();
            CacheHolder<TResult> holder = new CacheHolder<TResult>
            {
                Content = content,
                Timestamps = currentResults.ToArray(),
                Dependencies = dependenciesCopy.ToArray(),
            };

            Cache.Add(new CacheItem(key + keySuffix, holder), new CacheItemPolicy() { AbsoluteExpiration = DateTimeOffset.Now.AddHours(4)});
            return content;

        }

        private static string GetKeySuffix(List<Dependency> dependenciesCopy)
        {
            StringBuilder ret = new StringBuilder();
            foreach (var d in dependenciesCopy)
            {
                if (ret.Length > 0) ret.Append(',');
                if (d.OptionalKey == null)
                    ret.Append('S').Append(d.EntitySet);
                else
                {
                    ret.Append('E').Append(d.EntitySet);
                    if (d.OptionalKey is byte[])
                        ret.Append(Convert.ToBase64String((byte[]) d.OptionalKey));
                    else ret.Append(d.OptionalKey);
                }
            }

            return ret.ToString();
        }
    }

    public static class TimestampComparer
    {
        public static bool AreSame(byte[] one, byte[] two)
        {
            if (one == null || two == null || one.Length != two.Length)
                return false;

            int l = one.Length;
            for (int i = 0; i < l; i++)
                if (one[i] != two[i])
                    return false;

            return true;
        }
    }

    [Serializable]
    class CacheHolder<TResult>
    {
        public Dependency[] Dependencies;
        public byte[][] Timestamps;
        public TResult Content;
    }
}
