﻿namespace CacheInvalidation.EF
{
    using System;

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public class CacheInvalidationAttribute : Attribute
    {
        public readonly Type EntityType;
        public readonly string Property;

        public string CachingContext { get; set; }

        public CacheInvalidationAttribute(Type entityType)
        {
            if (entityType == null)
                throw new ArgumentNullException(nameof(entityType));

            EntityType = entityType;
        }

        public CacheInvalidationAttribute(Type entityType, string property)
            : this(entityType)
        {
            Property = property;
        }

    }
}
