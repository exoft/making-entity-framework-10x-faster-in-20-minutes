namespace CacheInvalidation.EF
{
    using System;
    using System.Data;
    using System.Data.Entity;
    using System.Data.SqlClient;

    public class SqlInvalidationStorageBuilder : IInvalidationStorageBuilder
    {
        private SqlConnection SqlConnection;
        private SqlTransaction DbTransaction;

        public SqlInvalidationStorageBuilder(SqlConnection sqlConnection, SqlTransaction dbTransaction)
        {
            SqlConnection = sqlConnection;
            DbTransaction = dbTransaction;
        }

        public static SqlInvalidationStorageBuilder FromDbContext(DbContext db)
        {
            var transaction = (SqlTransaction)db.Database.CurrentTransaction?.UnderlyingTransaction;
            return new SqlInvalidationStorageBuilder(
                (SqlConnection)db.Database.Connection,
                transaction);
        }


        public void PrepareSet(string setType)
        {
            if (SqlConnection.State == ConnectionState.Closed)
                SqlConnection.Open();

            string sql1 = string.Format(SQL_PrepareSet1, setType);
            string sql2 = string.Format(SQL_PrepareSet2, setType);
            foreach (var sql in new[] { sql1, sql2})
            {
                using (SqlCommand cmd = new SqlCommand(sql, SqlConnection))
                {
                    cmd.Transaction = DbTransaction;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        private const string SQL_PrepareEntity = @"
if (object_id('[{0}_InvalidationOfEntities]') is null)
Create Table [{0}_InvalidationOfEntities](
   Id {1} not null, 
   [timestamp] uniqueidentifier not null,
   Constraint [PK_{0}_InvalidationOfEntities] Primary Key (Id)
)";

        private const string SQL_PrepareSet1 = @"
if (object_id('[{0}_InvalidationOfSet]') is null)
Create Table [{0}_InvalidationOfSet](
   [timestamp] uniqueidentifier not null,
   Constraint [PK_{0}_InvalidationOfSet] Primary Key ([timestamp])
)";

        private const string SQL_PrepareSet2 = @"
declare @guid uniqueidentifier
set @guid = newid()
if not exists(select 1 from [{0}_InvalidationOfSet])
Insert [{0}_InvalidationOfSet]([timestamp]) Values(@guid)";

        public void PrepareEntity(string setType, ColumnMeta keyMeta)
        {
            if (SqlConnection.State == ConnectionState.Closed)
                SqlConnection.Open();

            string sql = string.Format(SQL_PrepareEntity, setType, GetSqlTypeName(keyMeta));
            using (SqlCommand cmd = new SqlCommand(sql, SqlConnection))
            {
                if (SqlConnection.State == ConnectionState.Closed)
                    SqlConnection.Open();

                cmd.Transaction = DbTransaction;
                cmd.ExecuteNonQuery();
            }
        }

        static string GetSqlTypeName(ColumnMeta keyMeta)
        {
            switch (keyMeta.Type)
            {
                case SupportedKeyTypes.Guid:
                    return "uniqueidentifier";

                case SupportedKeyTypes.Int16:
                    return "smallint";

                case SupportedKeyTypes.Int32:
                    return "int";

                case SupportedKeyTypes.Int64:
                    return "bigint";

                case SupportedKeyTypes.Binary:
                    return "varbinary(898)";

                case SupportedKeyTypes.String:
                    return "nvarchar(448)";

                default:
                    return null;
            }
        }
    }
}