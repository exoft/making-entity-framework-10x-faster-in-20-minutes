﻿using System.Data.SqlClient;
using System.Text;

namespace CacheInvalidation.EF
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Core.Metadata.Edm;
    using System.Data.Entity.Core.Objects;
    using System.Data.Entity.Infrastructure;
    using System.Diagnostics;
    using System.Linq;
    using System.Reflection;

    public class CacheInvalidator
    {
        public IInvalidationStorage InvalidationStorage { get; set; }
        public IInvalidationStorageBuilder InvalidationStorageBuilder { get; set; }

        public readonly DbContext Context;
        protected readonly Type ContextType;
        // Key: ContextType
        private static Dictionary<string, List<Item>> ItemsByContext = new Dictionary<string, List<Item>>();
        static readonly object Sync = new object();
        private List<Item> DependencyMetaList;

        public CacheInvalidator(DbContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            Context = context;
            ContextType = context.GetType();
            List<Item> list;
            if (!ItemsByContext.TryGetValue(ContextType.FullName, out list))
                list = BuildMeta();

            DependencyMetaList = list;
        }

        class Item
        {
            public Type ContextType;
            public Type EntityType;
            // Can be null
            public PropertyInfo Property;
            private static readonly object[] Args = new object[0];
            public Type TargetType;

            public object GetPropertyValue(object entity)
            {
                return Property.GetValue(entity, Args);
            }

            public string Dump()
            {
                return $"{EntityType.FullName}"
                    + (Property == null ? "" : ("::" + Property.Name))
                    + $" --> {TargetType.FullName}";

            }
        }

        public void AttachListener()
        {
            IObjectContextAdapter a = this.Context;
            a.ObjectContext.SavingChanges += ObjectContextOnSavingChanges;
/*
            a.ObjectContext.ObjectMaterialized += (sender, args) =>
            {
                Debug.WriteLine("Materialized: " + args.Entity);
                if (args.Entity.GetType().Name == "Organization" && Debugger.IsAttached) Debugger.Break();

            };
*/
        }

        public void PrepareStorage(IInvalidationStorageBuilder storageBuilder)
        {
            List<Item> items = DependencyMetaList;
            if (items == null)
                throw new InvalidOperationException("Wrong invalidator state. Please report");

            StringBuilder b = new StringBuilder($"{Context.GetType().Name} DB Prepared for invalidation: ");
            int n = 0;
            HashSet<string> prepared = new HashSet<string>();
            foreach (var item in items)
            {
                string info;
                if (item.Property == null)
                {
                    info = $"Set {item.TargetType.FullName}";
                    if (!prepared.Contains(info))
                        storageBuilder.PrepareSet(item.TargetType.FullName);
                }
                else
                {
                    info = $"Entities {item.TargetType.FullName}";
                    if (!prepared.Contains(info))
                        storageBuilder.PrepareEntity(item.TargetType.FullName, GetColumnInfo(item.Property));
                }
                prepared.Add(info);
            }

            b.Append(string.Join(", ", prepared.OrderBy(x => x)));
            Debug.WriteLine(b);
        }

        static ColumnMeta GetColumnInfo(PropertyInfo p)
        {
            SupportedKeyTypes t;
            var pt = p.PropertyType;
            if (pt == typeof(int) || pt == typeof(int?))
                t = SupportedKeyTypes.Int32;
            else if (pt == typeof(Int16) || pt == typeof(Int16?))
                t = SupportedKeyTypes.Int16;
            else if (pt == typeof(Guid) || pt == typeof(Guid?))
                t = SupportedKeyTypes.Guid;
            else if (pt == typeof(Int64) || pt == typeof(Int64?))
                t = SupportedKeyTypes.Int64;
            else if (pt == typeof(string))
                t = SupportedKeyTypes.String;
            else if (pt == typeof(byte[]))
                t = SupportedKeyTypes.Binary;
            else 
                throw new CacheInvalidationConfigurationException(
                    $"Unsupported invalidation using {pt}::{p.Name} of entity {p.DeclaringType}");

            return new ColumnMeta() {Type = t};
        }

        private void ObjectContextOnSavingChanges(object sender, EventArgs e)
        {
            var transaction = (SqlTransaction)this.Context.Database.CurrentTransaction?.UnderlyingTransaction;
            // Debug.WriteLine("Transaction during SAVING: " + transaction);

            // return;

            IObjectContextAdapter a = this.Context;
            var ctx = a.ObjectContext;
            var changes = ctx.ObjectStateManager.GetObjectStateEntries(EntityState.Added | EntityState.Deleted | EntityState.Modified);
            Dictionary<Type, List<object>> changedIdList = new Dictionary<Type, List<object>>();
            HashSet<Type> changedSetList = new HashSet<Type>();
            foreach (ObjectStateEntry entityState in changes)
            {
                var state = entityState.State;
                var key = entityState.EntityKey;
                var entity = entityState.Entity;
                var typeKey = entityState.Entity.GetType();
                foreach (var item in DependencyMetaList)
                {
                    if (typeKey == item.EntityType || item.EntityType.IsAssignableFrom(typeKey))
                    {
                        // Invalidate Set?
                        if (item.Property == null)
                        {
                            changedSetList.Add(item.TargetType);
                            continue;
                        }
                            
                        // Invalidate prev id?
                        if (state == EntityState.Modified || state == EntityState.Deleted)
                        {
                            var idOrigin = entityState.OriginalValues[item.Property.Name];
                            changedIdList.GetOrAddNew(item.TargetType).Add(idOrigin);
                        }
                        // Invalidate new Id?
                        if (state == EntityState.Added || state == EntityState.Modified)
                        {
                            var idCurrent = entityState.CurrentValues[item.Property.Name];
                            changedIdList.GetOrAddNew(item.TargetType).Add(idCurrent);
                        }
                    }
                }

                var modified = entityState.GetModifiedProperties();
                var modified2 = string.Join(", ", modified);

                // Debug.WriteLine($"{state}: {entity} {modified2}");
            }

            foreach (var type in changedSetList)
                this.InvalidationStorage.UpdateSet(type.FullName);

            foreach (var t1 in changedIdList)
            {
                Type type = t1.Key;
                List<object> idList = t1.Value;
                foreach (var id in idList)
                    this.InvalidationStorage.UpdateEntity(type.FullName, id);

            }

        }

        List<Item> BuildMeta()
        {
            var objectContext = ((IObjectContextAdapter) Context).ObjectContext;

            var nameTypes = objectContext.MetadataWorkspace.GetItems<EntityType>(DataSpace.OSpace);
            List<Type> types = new List<Type>();

            string assemblyName = Context.GetType().Assembly.FullName;
            List<Item> items = new List<Item>();
            foreach (var entityType in nameTypes)
            {
                var type = Type.GetType(entityType.FullName + "," + assemblyName, true);
                Debug.WriteLine($"TYPE: {entityType.BuiltInTypeKind}: {type}");
                types.Add(type);

                // Type Attribute
                {
                    var attrs = type
                        .GetCustomAttributes(typeof(CacheInvalidationAttribute), true)
                        .OfType<CacheInvalidationAttribute>();

                    foreach (var attr in attrs)
                    {
                        bool hasProperty = !string.IsNullOrEmpty(attr.Property);
                        var propertyInfo =
                                hasProperty
                                ? type.GetProperty(attr.Property)
                                : null;

                        if (hasProperty && propertyInfo == null)
                            throw new CacheInvalidationConfigurationException(
                                $"Property {attr.Property} is not found on type {type.FullName}");

                        Item item = new Item()
                        {
                            TargetType = attr.EntityType,
                            ContextType = ContextType,
                            EntityType = type,
                            Property = propertyInfo,
                        };

                        items.Add(item);
                    }
                }

                foreach (var propertyInfo in type.GetProperties())
                {
                    // Properties attribute
                    var attrs = propertyInfo
                        .GetCustomAttributes(typeof(CacheInvalidationAttribute), true)
                        .OfType<CacheInvalidationAttribute>();

                    foreach (var attr in attrs)
                    {
                        if (!string.IsNullOrEmpty(attr.Property))
                            throw new CacheInvalidationConfigurationException(
                                "Attribute on property should not specify property name");

                        Item item = new Item()
                        {
                            TargetType = attr.EntityType,
                            ContextType = ContextType,
                            EntityType = type,
                            Property = propertyInfo,
                        };

                        items.Add(item);
                    }



                }
            }

            lock(Sync)
            ItemsByContext[ContextType.FullName] = items;

            string sep = Environment.NewLine + "  * ";
            string info = $"{ContextType.FullName} invalidation rules: "
                + (items.Count == 0 ? "None" : (items.Count + sep))
                + string.Join(sep, items.Select(x => x.Dump()));

            Debug.WriteLine(info);
            return items;
        }


    }
}
