@set conf=Debug
@"C:\Program Files (x86)\MSBuild\14.0\Bin\amd64\msbuild.exe" CacheInvalidation.sln /t:Clean,Rebuild /p:Configuration=%conf% /p:DefineConstants="TRACE;DEBUG"
@
@cd SampleDb.Tests\redis-2.8\
@call restart.cmd
@cd ..\..
packages\NUnit.ConsoleRunner.3.6.0\tools\nunit3-console.exe --workers=1 --work=SampleDb.Tests\bin\Debug SampleDb.Tests\bin\Debug\SampleDb.Tests.exe