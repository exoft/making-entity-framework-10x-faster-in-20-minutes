﻿using System;
using StackExchange.Redis;

namespace CacheInvalidation.EF.Redis
{
    public class RedisInvalidationStorage : IInvalidationStorage, IInvalidationStorageBuilder
    {
        private ConfigurationOptions Options;

        public RedisInvalidationStorage(ConfigurationOptions options)
        {
            Options = options;
        }

        public void PrepareSet(string setType)
        {
            // Nothing to do
        }

        public void PrepareEntity(string setType, ColumnMeta keyMeta)
        {
            // Nothing to do
        }

        public void UpdateSet(string setType)
        {
            string key = setType + "_InvalidationOfSet";
            using (var con = ConnectionMultiplexer.Connect(Options))
            {
                var db = con.GetDatabase();
                var newValue = Guid.NewGuid().ToByteArray();
                db.HashSet(key, "any", newValue);
            }
        }

        public void UpdateEntity(string setType, object idEntity)
        {
            string key = setType + "_InvalidationEntity";
            using (var con = ConnectionMultiplexer.Connect(Options))
            {
                var db = con.GetDatabase();
                var newValue = Guid.NewGuid().ToByteArray();
                db.HashSet(key, IdToRedisValue(idEntity), newValue);
            }
        }

        public bool CheckSet(string setType, out byte[] timestamp)
        {
            string key = setType + "_InvalidationOfSet";
            using (var con = ConnectionMultiplexer.Connect(Options))
            {
                var db = con.GetDatabase();
                var value = (byte[]) db.HashGet(key, "any");
                if (value != null)
                {
                    timestamp = value;
                    return true;
                }
            }

            timestamp = null;
            return false;
        }

        public bool CheckEntity(string setType, object idEntity, out byte[] timestamp)
        {
            string key = setType + "_InvalidationEntity";
            using (var con = ConnectionMultiplexer.Connect(Options))
            {
                var db = con.GetDatabase();
                RedisValue idRedis = IdToRedisValue(idEntity);
                var value = (byte[])db.HashGet(key, idRedis);
                if (value != null)
                {
                    timestamp = value;
                    return true;
                }
            }

            timestamp = null;
            return false;
        }

        static RedisValue IdToRedisValue(object id)
        {
            if (id == null)
                throw new InvalidOperationException("id should not be null");

            if (id is int)
                return (int) id;

            else if (id is long)
                return (long) id;

            else if (id is short)
                return (short) id;

            else if (id is string)
                return (string) id;

            else if (id is byte[])
                return (byte[]) id;

            else if (id is Guid)
                return ((Guid) id).ToByteArray();

            else
                throw new ArgumentException("id of type " + id.GetType() + " is not supported");
        }
    }
}