namespace SampleDb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRegionalDivision : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RegionalDivision",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        IdParent = c.Int(nullable: false),
                        RegionDescription = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Organizations", t => t.Id)
                .ForeignKey("dbo.Organizations", t => t.IdParent)
                .Index(t => t.Id)
                .Index(t => t.IdParent);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RegionalDivision", "IdParent", "dbo.Organizations");
            DropForeignKey("dbo.RegionalDivision", "Id", "dbo.Organizations");
            DropIndex("dbo.RegionalDivision", new[] { "IdParent" });
            DropIndex("dbo.RegionalDivision", new[] { "Id" });
            DropTable("dbo.RegionalDivision");
        }
    }
}
