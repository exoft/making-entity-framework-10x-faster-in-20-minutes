namespace SampleDb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddCountry : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Countries",
                c => new
                {
                    Id = c.Short(nullable: false, identity: true),
                    LocalName = c.String(),
                    EnglishName = c.String(),
                    Flag = c.Binary(),
                })
                .PrimaryKey(t => t.Id);

        }

        public override void Down()
        {
            DropTable("dbo.Countries");
        }
    }
}
