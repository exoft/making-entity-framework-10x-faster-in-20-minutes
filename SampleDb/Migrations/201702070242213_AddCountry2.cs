namespace SampleDb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCountry2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Employee", "CountryId", c => c.Short());
            AddColumn("dbo.Organizations", "CountryId", c => c.Short());
            CreateIndex("dbo.Employee", "CountryId");
            CreateIndex("dbo.Organizations", "CountryId");
            AddForeignKey("dbo.Employee", "CountryId", "dbo.Countries", "Id");
            AddForeignKey("dbo.Organizations", "CountryId", "dbo.Countries", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Organizations", "CountryId", "dbo.Countries");
            DropForeignKey("dbo.Employee", "CountryId", "dbo.Countries");
            DropIndex("dbo.Organizations", new[] { "CountryId" });
            DropIndex("dbo.Employee", new[] { "CountryId" });
            DropColumn("dbo.Organizations", "CountryId");
            DropColumn("dbo.Employee", "CountryId");
        }
    }
}
