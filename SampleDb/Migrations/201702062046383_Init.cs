namespace SampleDb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Customer",
                c => new
                {
                    CustomerId = c.Int(nullable: false, identity: true),
                    Name = c.String(nullable: false, maxLength: 20),
                    Surname = c.String(nullable: false, maxLength: 20),
                })
                .PrimaryKey(t => t.CustomerId);

            CreateTable(
                "dbo.Dep",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    OrganizationId = c.Int(nullable: false),
                    HeadId = c.Int(),
                    ParentId = c.Int(),
                    Name = c.String(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Organizations", t => t.OrganizationId, cascadeDelete: true)
                .ForeignKey("dbo.Employee", t => t.HeadId)
                .ForeignKey("dbo.Dep", t => t.ParentId)
                .Index(t => t.OrganizationId)
                .Index(t => t.HeadId)
                .Index(t => t.ParentId);

            CreateTable(
                "dbo.Employee",
                c => new
                {
                    EmpId = c.Int(nullable: false, identity: true),
                    Name = c.String(nullable: false, maxLength: 20),
                    SurName = c.String(nullable: false, maxLength: 20),
                    OrganizationId = c.Int(nullable: false),
                    DepartmentId = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.EmpId)
                .ForeignKey("dbo.Organizations", t => t.OrganizationId, cascadeDelete: false)
                .ForeignKey("dbo.Dep", t => t.DepartmentId, cascadeDelete: false)
                .Index(t => t.OrganizationId)
                .Index(t => t.DepartmentId);

            CreateTable(
                "dbo.Organizations",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    DirectorId = c.Int(),
                    Title = c.String(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Employee", t => t.DirectorId)
                .Index(t => t.DirectorId);

            CreateTable(
                "dbo.Occupations",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    EmployeeId = c.Int(nullable: false),
                    ProjectId = c.Int(nullable: false),
                    Role = c.String(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Employee", t => t.EmployeeId, cascadeDelete: true)
                .ForeignKey("dbo.Projects", t => t.ProjectId, cascadeDelete: true)
                .Index(t => t.EmployeeId)
                .Index(t => t.ProjectId);

            CreateTable(
                "dbo.Projects",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    ProjectName = c.String(),
                    IdCustomer = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customer", t => t.IdCustomer, cascadeDelete: true)
                .Index(t => t.IdCustomer);

        }

        public override void Down()
        {
            DropForeignKey("dbo.Occupations", "ProjectId", "dbo.Projects");
            DropForeignKey("dbo.Projects", "IdCustomer", "dbo.Customer");
            DropForeignKey("dbo.Occupations", "EmployeeId", "dbo.Employee");
            DropForeignKey("dbo.Dep", "ParentId", "dbo.Dep");
            DropForeignKey("dbo.Dep", "HeadId", "dbo.Employee");
            DropForeignKey("dbo.Employee", "DepartmentId", "dbo.Dep");
            DropForeignKey("dbo.Employee", "OrganizationId", "dbo.Organizations");
            DropForeignKey("dbo.Organizations", "DirectorId", "dbo.Employee");
            DropForeignKey("dbo.Dep", "OrganizationId", "dbo.Organizations");
            DropIndex("dbo.Projects", new[] { "IdCustomer" });
            DropIndex("dbo.Occupations", new[] { "ProjectId" });
            DropIndex("dbo.Occupations", new[] { "EmployeeId" });
            DropIndex("dbo.Organizations", new[] { "DirectorId" });
            DropIndex("dbo.Employee", new[] { "DepartmentId" });
            DropIndex("dbo.Employee", new[] { "OrganizationId" });
            DropIndex("dbo.Dep", new[] { "ParentId" });
            DropIndex("dbo.Dep", new[] { "HeadId" });
            DropIndex("dbo.Dep", new[] { "OrganizationId" });
            DropTable("dbo.Projects");
            DropTable("dbo.Occupations");
            DropTable("dbo.Organizations");
            DropTable("dbo.Employee");
            DropTable("dbo.Dep");
            DropTable("dbo.Customer");
        }
    }
}
