using System.Data.Entity.Validation;
using System.Data.SqlClient;

namespace SampleDb.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Core.Metadata.Edm;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Migrations;
    using System.Diagnostics;
    using System.Linq;

    using CacheInvalidation.EF;

    using Entities;

    public sealed class Configuration : DbMigrationsConfiguration<EmployeeContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(EmployeeContext context)
        {
            var builder = SqlInvalidationStorageBuilder.FromDbContext(context);
            context.CacheInvalidator.PrepareStorage(builder);

            // context.Database.Log = s => Console.WriteLine(s);

            context.Organization.AddOrUpdate(o => o.Title, new Organization() { Title = "MI-6" });
            context.SaveChanges();
            var org = context.Organization.FirstOrDefault(x => x.Title == "MI-6");
            context.Organization.AddOrUpdate(o => o.Title, new RegionalDivision() { Title = "Torchwood", IdParent = org.Id});


            context.Department.AddOrUpdate(o => o.Name, new Department() { OrganizationId = org.Id, Name = "The Holding" });
            context.Department.AddOrUpdate(o => o.Name, new Department() { OrganizationId = org.Id, Name = "Board of directors" });
            context.SaveChanges();

            var root = context.Department.FirstOrDefault(x => x.Name == "The Holding");
            var directors = context.Department.FirstOrDefault(x => x.Name == "Board of directors");

            context.Employee.AddOrUpdate(x => x.SurName, new Employee()
            {
                OrganizationId = org.Id,
                DepartmentId = root.Id,
                Name = "James",
                SurName = "Bond",
            });

            context.Country.AddOrUpdate(x => x.EnglishName, new Country() { EnglishName = "Ukraine"});
            context.SaveChanges();
            SqlConnectionStringBuilder csb = new SqlConnectionStringBuilder(context.Database.Connection.ConnectionString);
            Debug.WriteLine($"Configuration.Seed completed on Server: {csb.DataSource} (DB is {csb.InitialCatalog})");


            // DumpMetaData(context);
        }

        protected void DumpMetaData(EmployeeContext context)
        {
            var objectContext = ((IObjectContextAdapter)context).ObjectContext;

            var nameTypes = objectContext.MetadataWorkspace.GetItems<EntityType>(DataSpace.OSpace);
            List<Type> types = new List<Type>();

            string assemblyName = context.GetType().Assembly.FullName;
            foreach (var entityType in nameTypes)
            {
                var type = Type.GetType(entityType.FullName + "," + assemblyName, true);
                Debug.WriteLine($"TYPE: {entityType.BuiltInTypeKind}: {type}");
                types.Add(type);
            }

            var container = objectContext.MetadataWorkspace.GetEntityContainer(objectContext.DefaultContainerName, DataSpace.CSpace);
            int n = 0;
            foreach (var set in container.BaseEntitySets)
            {
                Console.WriteLine($"{++n} {set.Name}: {set.Table} {set.BuiltInTypeKind}");

                var keys = set.ElementType.KeyMembers;
                foreach (var metaproperty in set.MetadataProperties)
                {
                    var p = metaproperty;
                    Console.WriteLine($"  * {p.Name} {p.BuiltInTypeKind} {p.TypeUsage} = '{p.Value}'");

                    var props2 = p.MetadataProperties;
                    int n2 = props2.Count;
                    foreach (MetadataProperty p2 in props2)
                    {
                        Console.WriteLine($"   .{p2.Name}='{p2.Value}' ({p2.TypeUsage}, {p2.BuiltInTypeKind}, {p2.PropertyKind})");
                    }

                }

                Console.WriteLine();
            }

        }


    }
}
