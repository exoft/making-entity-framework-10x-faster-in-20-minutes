﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace SampleDb.Logica
{
    using System.Runtime.Caching;

    using CacheInvalidation.EF;

    using Entities;

    public partial class MyAccess
    {
        private EmployeeContext Context;


        public MyAccess(EmployeeContext context)
        {
            Context = context;
        }


        public Organization GetOrganization_WithDepartments_AndCountryFlag2(int idOrganization)
        {
            return Context.Organization.AsNoTracking()
                .Include("Director")
                .Include("Country")
                .Include("Departments")
                .Include("Departments.Head")
                .FirstOrDefault(x => x.Id == idOrganization);
        }




        public List<Country> GetAllCountries_WithoutFlag()
        {
            IQueryable<Country> query =
                from x in Context.Country.AsNoTracking()
                orderby x.EnglishName
                select new Country
                {
                    Id = x.Id,
                    LocalName = x.LocalName,
                    EnglishName = x.EnglishName
                };

            return query.ToList();
        }


    }



}
