﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CacheInvalidation.EF;
using SampleDb.Entities;

namespace SampleDb.Logica
{
    partial class MyAccess
    {
        public Organization GetOrganization_WithDepartments_AndCountryFlag(int idOrganization)
        {
            var dependencies = new[]
            {
                new Dependency<Organization, int>(idOrganization),
                Dependency<Country>.EntireSet,
            };

            return
                dependencies.LazyLoad<Organization>(
                    "Organization With Departments And Country's Flag",
                    Context.CacheInvalidator, () =>
                    {
                        return Context.Organization.AsNoTracking()
                            .Include("Director")
                            .Include("Country")
                            .Include("Departments")
                            .Include("Departments.Head")
                            .FirstOrDefault(x => x.Id == idOrganization);
                    });
        }

        public Country GetCountry_WithFlag2(short idCountry)
        {
            var dependencies = new[]
            {
                new Dependency<Country, short>(idCountry)
            };

            return
                dependencies.LazyLoad<Country>(
                    "Country with flag",
                    Context.CacheInvalidator, () =>
                    {
                        return Context.Country.FirstOrDefault(x => x.Id == idCountry);
                    });
        }

        public List<Country> GetAllCountries_WithoutFlag2()
        {
            return
                Dependency<Country>.EntireSet.LazyLoad(
                "All Countries without Flag",
                Context.CacheInvalidator, () =>
                {
                    IQueryable<Country> query =
                        from x in Context.Country.AsNoTracking()
                        orderby x.EnglishName
                        select new Country
                        {
                            Id = x.Id,
                            LocalName = x.LocalName,
                            EnglishName = x.EnglishName
                        };

                    return query.ToList();
                });
        }





    }
}
