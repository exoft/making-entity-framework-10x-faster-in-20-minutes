namespace SampleDb.Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using CacheInvalidation.EF;

    public class Organization
    {
        [CacheInvalidation(typeof(Organization))]
        public int Id { get; set; }

        [ForeignKey("Director")]
        public int? DirectorId { get; set; }

        public Employee Director { get; set; }

        [InverseProperty("Organization")]
        public virtual ICollection<Department> Departments { get; set; }


        [InverseProperty("Organization")]
        public virtual ICollection<Employee> Employees { get; set; }

        [InverseProperty("ParentOrganization")]
        public virtual ICollection<RegionalDivision> SubDivisions { get; set; }


        [Required]
        public string Title { get; set; }

        [ForeignKey("Country")]
        public short? CountryId { get; set; }
        public Country Country { get; set; }


    }

    [Table("RegionalDivision")]
    public class RegionalDivision : Organization
    {
        [ForeignKey("ParentOrganization")]
        public int IdParent { get; set; }

        public Organization ParentOrganization { get; set; }

        public string RegionDescription { get; set; }

    }
}