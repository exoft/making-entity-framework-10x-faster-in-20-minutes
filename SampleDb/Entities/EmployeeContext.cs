namespace SampleDb.Entities
{
    using System;
    using System.Data.Common;
    using System.Data.Entity;
    using System.Data.Entity.Core.Metadata.Edm;
    using System.Data.Entity.Core.Objects;
    using System.Data.Entity.Infrastructure;
    using System.Linq;

    using global::CacheInvalidation.EF;

    using Migrations;

    public class EmployeeContext : DbContext
    {
        public CacheInvalidator CacheInvalidator { get; set; }

        public EmployeeContext(DbConnection existingConnection, bool attachDefaultInvalidation = true, bool ownConnection = true) : base(existingConnection, ownConnection)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<EmployeeContext, Configuration>());
            if (attachDefaultInvalidation)
                CacheInvalidator = ConfigureInvalidation();
        }

        public EmployeeContext() : base("CacheInvalidation.SampleDb")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<EmployeeContext, Configuration>());
            CacheInvalidator = ConfigureInvalidation();
        }

        CacheInvalidator ConfigureInvalidation()
        {
            var ret = new CacheInvalidator(this)
            {
                InvalidationStorage = SqlInvalidationStorage.FromDbContext(this),
                InvalidationStorageBuilder = SqlInvalidationStorageBuilder.FromDbContext(this),
            };

            ret.AttachListener();
            return ret;
        }



        public DbSet<Employee> Employee { get; set; }

        public DbSet<Customer> Customer { get; set; }

        public DbSet<Project> Project { get; set; }

        public DbSet<Occupation> Occupation { get; set; }

        public DbSet<Organization> Organization { get; set; }

        public DbSet<Department> Department { get; set; }

        public DbSet<Country> Country { get; set; }



    }
}