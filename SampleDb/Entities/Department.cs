namespace SampleDb.Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using CacheInvalidation.EF;

    public class Department
    {
        public int Id { get; set; }

        [ForeignKey("Organization")]

        [CacheInvalidation(typeof(Organization))]
        public int OrganizationId { get; set; }

        public virtual Organization Organization { get; set; }

        [ForeignKey("Head")]
        public int? HeadId { get; set; }

        public virtual Employee Head { get; set; }

        [ForeignKey("Parent")]
        public int? ParentId { get; set; }

        public virtual Department Parent { get; set; }

        [InverseProperty("Department")]
        public virtual ICollection<Employee> Employees { get; set; }

        [Required]
        public string Name { get; set; }


    }
}