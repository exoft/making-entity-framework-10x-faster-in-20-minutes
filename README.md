### CacheInvalidation.EF is a smarty cache invalidation
It intended for accurate and correct invalidation of query results in OLTP scenarios. It is especially designed for Entity Framework.
### TL;DR
`[CacheInvalidation]` attribute defines invalidation rules.
`CacheInvalidator` performs flushing of changed data based on ObjectStateManager.
`DependenciesExtentions.LazyLoad` performs pre-checking timestamps of cached results and optionally invokes specified db-query(ies)
### SampleDb.Tests
It requires a LocalDB of any version
### Limitations
- Not all of EDM-types are supported as entity keys. Supported types are: Int16, Int32, Int64, Guid, String, Binary.
- Just MS SQL Server DB is supported and tested. It seems SQL CE 4.0 should be also supported, but Compact Edition was not tested.
- Composite keys are not supported yet.
### Drawback
![drawback is about 20-30% for simple add/update](https://bytebucket.org/exoft/making-entity-framework-10x-faster-in-20-minutes/raw/master/Images/Drawback.png)
### Performance Gain
See output of `Test_A1_Query_Perfomance_Improvement()`
### TODO
- Support of composite key.
- Tests with SQL CE.
- Support of another RDBMS.
- Support of the all another EDM-types as a key type.
- Add independent scopes per entity or set
