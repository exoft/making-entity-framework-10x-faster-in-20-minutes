﻿using System;
using System.Data.SqlClient;
using NUnit.Framework;
using SampleDb.Tests.Utils;

namespace SampleDb.Tests
{
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [TestFixture]
    public class Utilities
    {
        [Category("Maintenance")]
        [Explicit]
        [Test]
        public void Drop_CacheInvlidation_Temp_Databases()
        {
            SqlConnection con = new SqlConnection(DbEnv.MasterConnectionString);
            var dbs = con.GetDatabases();
            foreach (var name in dbs.Keys)
            {
                if (name.StartsWith("CacheInvlidation(Stress) "))
                {
                    Console.WriteLine("Deleting DB {0}", name);
                    SqlExtentions.DropDatabase_And_CloseConnections(
                        DbEnv.MasterConnectionString,
                        name,
                        throwOnError: false);
                }
            }
        }

        [Category("Maintenance")]
        [Explicit]
        [Test]
        public void Show_RAM_Disks()
        {
            Debug.WriteLine("All Local H-Disks");
            DriveInfo[] drives = DriveInfo.GetDrives();
            foreach (var i in drives)
            {
                string label = null;
                long? free = null;
                if (i.DriveType == DriveType.Fixed)
                {
                    try
                    {
                        label = i.VolumeLabel;
                        free = i.AvailableFreeSpace;
                    }
                    catch (Exception)
                    {
                    }
                }

                bool isRamDrive = ("RamDrive".Equals(label, StringComparison.InvariantCultureIgnoreCase)) || i.DriveType == DriveType.Ram;
                Debug.WriteLine(i.RootDirectory + ": " + i.DriveType 
                    + (isRamDrive ? ", RAM-DRIVE: " + (free.Value / 1024/1024) + " MB free":""));
            }
        }

        [Test]
        public void Show_Local_Serveres()
        {

            var list = SqlDiscovery.GetLocalDbAndServerList();
            list["(LocalDB)\\MSSqlLocalDB"] = null;
            Debug.WriteLine($"Found {list.Count} servers: {list.AsHumanReadableString()}");
            Debug.WriteLine("");
            var ordered = list.OrderByVersionDesc();
            int total = ordered.Count(), alive = 0, sysadmin = 0;
            Parallel.ForEach(ordered, (s) =>
            {
                string cs = $"Data Source={s}; Integrated Security=true";
                string v = list[s]?.ToString() ?? "N/A";
                StringBuilder report = new StringBuilder();
                report.AppendLine($"SERVER '{s}' Ver: {v}");
                try
                {
                    using (SqlConnection con = new SqlConnection(cs))
                    {
                        report.AppendLine("Version .........: " + con.GetServerVersion());
                        alive++;
                        report.AppendLine("Long Version ....: " + con.GetServerVersionLong());
                        report.AppendLine("Product Level ...: " + con.GetServerProductLevel());
                        report.AppendLine("Edition .........: " + con.GetServerEdition());
                        report.AppendLine("Engine Edition ..: " + con.GetServerEngineEdition());
                        report.AppendLine("Security Mode ...: " + con.GetServerSecurityMode());
                        report.AppendLine("Is LocalDB ......: " + con.IsLocalDB());
                        var roles = con.IsInFixedRoles();
                        var isSysAdmin = ((roles & SqlExtentions.FixedServerRoles.SysAdmin) != 0);
                        var rolesString = isSysAdmin ? "Sys Admin" : roles.ToString();
                        report.AppendLine("Roles ...........: " + rolesString);
                        var dbList = con.GetDatabases();
                        report.AppendLine("Databases .......: " + dbList.Count + " ("+ dbList.Sum(x => x.Value) + " Kb)");
                        if (isSysAdmin) sysadmin++;
                    }
                }
                catch (Exception ex)
                {
                    report.AppendLine("Exception ..........: " + ex.GetExeptionDigest());
                }

                Debug.WriteLine(report);


            });

            Debug.WriteLine($"Servers: {total}. Alive: {alive}. SysAdmins: {sysadmin}.");
        }

    }
}