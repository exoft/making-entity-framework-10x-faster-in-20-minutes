﻿using System;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework.Constraints;
using SampleDb.Entities;
using SampleDb.Migrations;

namespace SampleDb.Tests
{
    using System.Linq;

    using Utils;

    class DbEnv
    {
        public const string MasterConnectionString = "Data Source=(localdb)\\MSSQLLocalDB;Integrated Security=true;";

        public static string ConnectionString_InProduction
        {
            get
            {
                string cs =
                    "Data Source = (LocalDb)\\MSSQLLocalDB; " +
                    "Initial Catalog = " + DbName + "; " +
                    "Integrated Security = SSPI; ";
                return cs;
            }
        }


        static DbEnv()
        {
            AppDomain.CurrentDomain.UnhandledException += (sender, args) =>
            {
                Debug.WriteLine("UNHANDLED Exception:" + Environment.NewLine + args.ExceptionObject);
            };

            SetupDebugListener();


        }

        public static void SetupDebugListener()
        {
            var name = Process.GetCurrentProcess()?.ProcessName ?? "";
            Console.WriteLine("Process: " + name);
            if (name.ToLower().StartsWith("nunit-agent"))
            {
                Debug.Listeners.Add(new ConsoleTraceListener());
            }
        }


        public static SqlConnection CreateSqlConnection(string fileName)
        {
            try
            {
                Directory.CreateDirectory(Path.GetDirectoryName(fileName));
            }
            catch { }


            if (!File.Exists(fileName))
            {
                CreateDatabase(MasterConnectionString, DbName, fileName);
            }
            var cs = ConnectionString_InProduction;

            SqlConnection sql = new SqlConnection(cs);
            return sql;
        }


        public static void CreateDatabase(string csMaster, string dbName, string fullSdfName)
        {
            string sql =
                string.Format(
                    "Create Database [{0}] On (Name='{0}', FileName='{1}')",
                    dbName,
                    fullSdfName);

            using (SqlConnection con = new SqlConnection(csMaster))
            using (SqlCommand cmd = new SqlCommand(sql, con))
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public static string NextDb()
        {
            lock (FolderLock)
            {
                var now = DateTime.Now.ToString("yyyy-MM-dd-HH.mm.ss");
                var root = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                    "DB Tests");

                var ramDrives = RamDriveInfo.FindRamDrives(300);
                if (ramDrives.Any())
                    root = ramDrives.First().RootDirectory.FullName;

                string name = Path.Combine(
                    root,
                    typeof (EmployeeContext).Name + " " + now
                    );

                DbName = "CacheInvlidation(Stress) " + now;
                Folder = name;
                return Folder;

            }
        }

        public static string Folder;
        public static string DbName;
        static object FolderLock = new object();

        public static void CreateDb(string cs)
        {
            Debug.WriteLine("Launching Migrator");
            var configuration = new Configuration();
            configuration.AutomaticMigrationsEnabled = true;
            configuration.TargetDatabase = new DbConnectionInfo(cs, "System.Data.SqlClient");
            var migrator = new DbMigrator(configuration);
            migrator.Update();
        }
    }
}
