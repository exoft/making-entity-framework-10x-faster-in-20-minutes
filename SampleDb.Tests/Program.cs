﻿namespace SampleDb.Tests
{
    using System;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;

    using CacheInvalidation.EF;

    using Logica;

    using SampleDb.Entities;

    class Program
    {
        static void Main(string[] args)
        {
            EmployeeContext db = new EmployeeContext();
            db = new EmployeeContext();
            db = new EmployeeContext();
            db = new EmployeeContext();
            db = new EmployeeContext();


            TestInvalidationStorage();

            db.Database.Log = s => Console.Write(s);
            MyAccess ma = new MyAccess(db);
            var org = ma.GetOrganization_WithDepartments_AndCountryFlag(1);
            org.ToString();



            /*
                        db.Customer.AddOrUpdate(new Customer() { Name = "J", Surname = "Doe"});
                        var emp = new Employee() { Name = "Crazy", SurName = "Monkey"};
                        db.Employee.AddOrUpdate(emp);
                        db.Project.AddOrUpdate(new Project() { });
                        db.SaveChanges();
            */
        }

        static void TestInvalidationStorage()
        {
            EmployeeContext db = new EmployeeContext();

            SqlInvalidationStorageBuilder b = new SqlInvalidationStorageBuilder(
                (SqlConnection)db.Database.Connection,
                (SqlTransaction)db.Database.CurrentTransaction?.UnderlyingTransaction);

            b.PrepareSet("UnitTest.Set");
            b.PrepareEntity("UnitTest.Entity(Int16)", new ColumnMeta() { Type = SupportedKeyTypes.Int16 });
            b.PrepareEntity("UnitTest.Entity(Int32)", new ColumnMeta() { Type = SupportedKeyTypes.Int32 });
            b.PrepareEntity("UnitTest.Entity(Int64)", new ColumnMeta() { Type = SupportedKeyTypes.Int64 });
            b.PrepareEntity("UnitTest.Entity(Guid)", new ColumnMeta() { Type = SupportedKeyTypes.Guid });
            b.PrepareEntity("UnitTest.Entity(String)", new ColumnMeta() { Type = SupportedKeyTypes.String });
            b.PrepareEntity("UnitTest.Entity(Binary)", new ColumnMeta() { Type = SupportedKeyTypes.Binary });

            SqlInvalidationStorage s = SqlInvalidationStorage.FromDbContext(db);

            byte[] id = new byte[16];
            id[0] = 1;
            s.UpdateEntity("UnitTest.Entity(Guid)", new Guid(id));
            s.UpdateEntity("UnitTest.Entity(Int16)", (Int16)16);
            s.UpdateEntity("UnitTest.Entity(Int32)", 32);
            s.UpdateEntity("UnitTest.Entity(Int64)", 64L);
            s.UpdateEntity("UnitTest.Entity(String)", "A KEY");
            s.UpdateEntity("UnitTest.Entity(Binary)", id);
            s.UpdateSet("UnitTest.Set");

            byte[] bytes;
            s.CheckEntity("UnitTest.Entity(Guid)", new Guid(id), out bytes);
            Console.WriteLine("Guid: " + string.Join("", bytes.Select(x => x.ToString("X2"))));


        }
    }
}
