﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework.Constraints;
using SampleDb.Entities;
using SampleDb.Tests.Utils;

namespace SampleDb.Tests
{
    using System.Data.SqlClient;

    using Dapper;

    class StressSeeder
    {
        private const int RootDepartments = 5;
        private const int SubDepartments = 15;
        private const int EmployeesPerDepartment = 10;

        public static void Seed(Func<EmployeeContext> getContext)
        {
            for (int i = 1; i <= 1; i++)
            {
                var organizationNames = GetOrganizations();
                foreach (var organizationName in organizationNames)
                {
                    var ctx0 = getContext();
                    Organization o = new Organization()
                    {
                        Title = organizationName+" #" +i,
                    };
                    ctx0.Organization.Add(o);
                    ctx0.SaveChanges();

                    // Root Departments
                    for (int d1 = 1; d1 <= RootDepartments; d1++)
                    {
                        SqlConnection con = new SqlConnection(DbEnv.ConnectionString_InProduction);
                        con.Open();
                        int idDep1 = InsertDepartment(con, o.Id, "Root Department " + d1, null);

                        // sub departments
                        for (int d2 = 65; d2 <= 65+SubDepartments-1; d2++)
                        {
                            var idDep2 = InsertDepartment(con, o.Id, "Department " + d1 + "." + ((char) d2), idDep1);
                            var idHead = CreateEmployee(con, "The", "Boss", o.Id, idDep2, null);
                            var sqlSetHead = "Update dbo.Departments set HeadId = @HeadId Where Id=@Id";
                            ctx0.Database.Connection.Execute(sqlSetHead, new
                            {
                                HeadId = idHead,
                                Id = idDep2
                            });

                            // employees
                            for (int u = 1; u < EmployeesPerDepartment; u++)
                            {
                                string name = "Worker " + u;
                                string surName = "Doe";
                                int idOrg = o.Id;
                                int idDep = idDep2;
                                int? idCountry = (int?) null;
                                CreateEmployee(con, name, surName, idOrg, idDep, idCountry);
                            }
                        }

                        con.Close();
                    }

                    if (ctx0.CacheInvalidator != null)
                    ctx0.CacheInvalidator.InvalidationStorage.UpdateEntity(
                        typeof(Organization).FullName,
                        o.Id
                        );

                    ctx0.Dispose();
                }


            }
        }

        private static int CreateEmployee(SqlConnection con, 
            string name, 
            string surName, 
            int idOrg, 
            int idDep,
            int? idCountry)
        {
            int id = con.ExecuteScalar<int>(@"
INSERT INTO [Employee]
           ([Name]
           ,[SurName]
           ,[OrganizationId]
           ,[DepartmentId]
           ,[CountryId])
     VALUES
           (@name, @sur, @idOrg, @idDep, @idCountry);

select scope_identity()
", new
            {
                name = name,
                sur = surName,
                idOrg = idOrg,
                idDep = idDep,
                idCountry = idCountry
            });

            return id;
        }

        public static void DumpTotals(EmployeeContext ctx)
        {
            Debug.WriteLine($"Oranizations: {ctx.Organization.Count()}");
            Debug.WriteLine($" Departments: {ctx.Department.Count()}");
            Debug.WriteLine($"   Employees: {ctx.Employee.Count()}");

            int dbSize;
            ctx.Database.Connection.AsSqlConnection().GetDatabases().TryGetValue(DbEnv.DbName, out dbSize);
            string dbSize2 = dbSize == 0 ? "N/A" : dbSize.ToString();
            Debug.WriteLine($"     DB Size: {dbSize2} K");
        }

        private static int InsertDepartment(SqlConnection con, int oId, string name, int? idParent)
        {
            string sql = @"
set nocount on
INSERT [dbo].[Departments]([OrganizationId], [ParentId], [Name])
VALUES(@idOrg, @idParent, @name)
/*
SELECT [Id]
FROM [dbo].[Departments]
WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
*/

select scope_identity()";

            var id = con.ExecuteScalar<int>(sql, new
            {
                idOrg = oId,
                idParent = idParent,
                name = name
            });

            return id;
        }

        static List<string> GetOrganizations()
        {
            return @"Asia-Pacific Economic Cooperation
Commonwealth of Nations
Shanghai Cooperation Organisation
South Asian Association for Regional Cooperation
Organization of American States
NATO
African Union
ASEAN
European Union
European Economic Area
North American Free Trade Agreement
Economic Cooperation Organization
Arab League
Union of South American Nations
OPEC
Mercosur/Mercosul
Collective Security Treaty Organisation
Eurasian Economic Union
Central American Integration System
Association of Caribbean States
CARICOM

Arab Maghreb Union (UMA)
Common Market for Eastern and Southern Africa (COMESA)
Community of Sahel- Saharan States (CEN-SAD)
East African Community (EAC)
Economic Community of Central African States (ECCAS)
Economic Community of West African States (ECOWAS)
Intergovernmental Authority on Development (IGAD)
Southern Africa Development Community (SADC)
Economic and Monetary Community of Central Africa
West African Economic and Monetary Union
International Conference on The Great Lakes Region
Indian Ocean Commission
Mano River Union
Senegal River Basin Development Authority
Southern African Customs Union

"
                .Split('\r', '\n')
                .Select(x => x.Trim())
                .Where(x => x.Trim().Length > 0)
                .ToList();
        }

    }
}

