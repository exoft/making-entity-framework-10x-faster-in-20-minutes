﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDb.Tests
{
    using System.Data.SqlClient;

    using Entities;

    class UnitOfDrawbackAnalizer
    {
        static Random Rand = new Random();

        public static void Implementation(EmployeeContext context, int idOrganization, int idDepartment)
        {
            var emp = new Employee()
            {
                OrganizationId = idOrganization,
                DepartmentId = idDepartment,
                Name = Rand.Next(0, 100000).ToString(),
                SurName = Rand.Next(0, 100000).ToString()
            };
            context.Employee.Add(emp);
            context.SaveChanges();
            emp.Name = "Other-)";
            context.SaveChanges();
            context.Employee.Remove(emp);
            context.SaveChanges();
        }

    }
}
