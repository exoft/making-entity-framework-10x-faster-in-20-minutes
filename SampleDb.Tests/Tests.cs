﻿using System;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using CacheInvalidation.EF;
using CacheInvalidation.EF.Redis;
using NUnit.Framework;
using SampleDb.Entities;
using SampleDb.Logica;
using SampleDb.Tests.Utils;
using StackExchange.Redis;

namespace SampleDb.Tests
{
    using System.Data.SqlClient;

    [TestFixture]
    public class Tests
    {
        private string _fileName;

        [TestFixtureSetUp]
        public void OneTimeSetUp()
        {
            _fileName = Path.Combine(DbEnv.NextDb(), DbEnv.DbName + ".sdf");
        }

        [TestFixtureTearDown]
        public void OneTimeTeaDown()
        {
            var ctx = Context_With_Redis_Invalidation();
            StressSeeder.DumpTotals(ctx);

            return;
            SqlExtentions.DropDatabase_And_CloseConnections(
                DbEnv.MasterConnectionString,
                DbEnv.DbName);
        }


        [Test]
        public void Test_1_Create_Empty_Unique_DB()
        {
            var c = DbEnv.CreateSqlConnection(_fileName);
            c.SetAutoShrinkProperty(needAutoShrink: false);
            Debug.WriteLine("Database ......... : " + c.GetCurrentDatabaseName());
            Debug.WriteLine("Recovery Mode .... : " + c.GetDatabaseRecoveryMode());
            Debug.WriteLine("Auto Shrink ...... : " + c.GetAutoShrinkProperty());
            Debug.WriteLine("Server Version ... : " + c.GetServerVersion());
            Debug.WriteLine("Server ........... : " + new SqlConnectionStringBuilder(c.ConnectionString).DataSource);

            Debug.WriteLine("Location: " + DbEnv.Folder);

            c.Dispose();
        }

        [Test]
        public void Test_2_Schema_Prepared()
        {
            using (var c = DbEnv.CreateSqlConnection(_fileName))
            {
                Debug.WriteLine("2-Database: " + c.GetCurrentDatabaseName());
                DbEnv.CreateDb(DbEnv.ConnectionString_InProduction);
            }


            using (var ctx = Context_With_Redis_Invalidation())
            {
                DbEnv.CreateDb(DbEnv.ConnectionString_InProduction);
            }

        }

        [Test]
        public void Test_3_Seed_Had_Triggered()
        {
            using (var db = Context_With_Same_DB_Invalidation())
            {
                CollectionAssert.IsNotEmpty(db.Country.ToList());
            }
        }

        [Test]
        public void Test_4_Invalidation_With_Redis_Works_Propertly()
        {

            using (var ctx = Context_With_Redis_Invalidation())
            {

                Debug.WriteLine("3-Database: " + ctx.Database.Connection.AsSqlConnection().GetCurrentDatabaseName());
                DbEnv.CreateDb(DbEnv.ConnectionString_InProduction);


                byte[] before;
                bool okBefore = ctx.CacheInvalidator.InvalidationStorage.CheckSet(typeof(Country).FullName, out before);
                var country = new Country();
                ctx.Country.Add(country);
                ctx.SaveChanges();
                byte[] after;
                bool okAfter = ctx.CacheInvalidator.InvalidationStorage.CheckSet(typeof(Country).FullName, out after);
                Assert.IsTrue(okAfter);
                Assert.IsNotNull(after);
                CollectionAssert.IsNotEmpty(after);
            }
        }

        [Test]
        public void Test_5_Hierarchy_Flushes_Properly()
        {
            using (var ctx = Context_With_Redis_Invalidation())
            {
                var anyOrg = ctx.Organization.First();
                var div = new RegionalDivision {Title = "New Country", IdParent = anyOrg.Id};
                ctx.Organization.Add(div);
                ctx.SaveChanges();

                byte[] before;
                var okBefore = ctx.CacheInvalidator.InvalidationStorage.CheckEntity(
                    typeof(Organization).FullName, div.Id, out before);

                div.Title = "Another Country";
                ctx.SaveChanges();
                byte[] after1;
                var okAfter1 = ctx.CacheInvalidator.InvalidationStorage.CheckEntity(
                    typeof(Organization).FullName, div.Id, out after1);

                var copy = ctx.Organization.First(x => x.Id == div.Id);
                copy.Title = "3rd Country";
                ctx.SaveChanges();

                byte[] after2;
                var okAfter2 = ctx.CacheInvalidator.InvalidationStorage.CheckEntity(
                    typeof(Organization).FullName, div.Id, out after2);

                CollectionAssert.IsNotEmpty(after1);
                CollectionAssert.IsNotEmpty(after2);
                CollectionAssert.AreNotEqual(after1, after2);
            }
        }

        [Test]
        public void Test_6_Seed_Stress_Data()
        {
            using (var ctx = Context_With_Same_DB_Invalidation())
            {
                Debug.WriteLine("3-Database: " + ctx.Database.Connection.AsSqlConnection().GetCurrentDatabaseName());
                DbEnv.CreateDb(DbEnv.ConnectionString_InProduction);
                StressSeeder.Seed(() => Context_With_Same_DB_Invalidation());
            }

        }

        [Test]
        public void Test_7_Shrink_After_Seed()
        {
            using (SqlConnection master = new SqlConnection(DbEnv.MasterConnectionString))
            {

                Action<string> showSize = (string caption) =>
                {
                    long? size = master.GetDatabaseSize(DbEnv.DbName);
                    Debug.WriteLine("DB size {0}: {1}", caption,
                        size.HasValue ? size + " Kb" : "N/A");
                };

                showSize("after seed");
                master.ShrinkDatabase(DbEnv.DbName, SqlExtentions.ShrinkOptions.TruncateOnly);
                showSize("after truncate");
                master.ShrinkDatabase(DbEnv.DbName, SqlExtentions.ShrinkOptions.ShinkAndTruncate);
                showSize("after shrink");
            }
        }

        [Test]
        public void Test_A1_Calculate_Drawback()
        {
            Random rand = new Random(1);
            var ctx0 = Context_Without_Invalidation();
            var info0 = (
                from o in ctx0.Organization.Where(x => x.Departments.Count > 0)
                select new {org = o, dep = o.Departments.FirstOrDefault()}).First();

            ThreadLocal<EmployeeContext> ctxLocal = new ThreadLocal<EmployeeContext>(
                    () => Context_Without_Invalidation());

            Func<Func<EmployeeContext>> init1 = () =>
            {
                Func<EmployeeContext> ret = () => ctxLocal.Value;
                return ret;
            };

            Func<Func<EmployeeContext>> init2_without_invalidation = () =>
            {
                Func<EmployeeContext> ret = () => Context_Without_Invalidation();
                return ret;
            };

            Func<Func<EmployeeContext>> init2_with_default_invalidation = () =>
            {
                Func<EmployeeContext> ret = () => Context_With_Same_DB_Invalidation();
                return ret;
            };

            Func<Func<EmployeeContext>> init2_with_redis_invalidation = () =>
            {
                Func<EmployeeContext> ret = () => Context_With_Same_DB_Invalidation();
                return ret;
            };


            Action<Func<EmployeeContext>> loobBody1 = getContext =>
            {
                var ctx = getContext();
                UnitOfDrawbackAnalizer.Implementation(
                    ctx, info0.org.Id, info0.dep.Id);
            };

            Action<Func<EmployeeContext>> loobBody2 = getContext =>
            {
                using (var ctx = getContext())
                    UnitOfDrawbackAnalizer.Implementation(
                        ctx, info0.org.Id, info0.dep.Id);
            };

            Action<Func<EmployeeContext>> dispose = getContext =>
            {
            };

            var ignored = StressIt.Run(init1, loobBody1, dispose, 1, 111);
            ignored = StressIt.Run(init2_without_invalidation, loobBody2, dispose, 1, 111);

            var tests = new[]
            {
                new
                {
                    Caption = "WITHOUT INVALIDATION",
                    Init = init2_without_invalidation,
                    LoopBody = loobBody2,
                    Dispose = dispose,
                },

                new
                {
                    Caption = "DEFAULT Invalidation (in the same DB)",
                    Init = init2_with_default_invalidation,
                    LoopBody = loobBody2,
                    Dispose = dispose,
                },

                new
                {
                    Caption = "Invalidation using Redis",
                    Init = init2_with_redis_invalidation,
                    LoopBody = loobBody2,
                    Dispose = dispose,
                },

            };

            foreach (var test in tests)
            {
                var ignored2 = StressIt.Run(test.Init, test.LoopBody, test.Dispose, 1, 111);
                Debug.WriteLine(test.Caption);
                foreach (int threads in new[] { 1, 4 })
                {
                    var report = StressIt.Run(
                        test.Init,
                        test.LoopBody,
                        test.Dispose,
                        threads,
                        3000
                    );
                    Debug.WriteLine(" * " + threads + " threads. " + report.AsShort());
                }

            }

        }

        [Test]
        public void Test_A1_Query_Perfomance_Improvement()
        {
            Func<Func<EmployeeContext>> init = () =>
            {
                Func<EmployeeContext> ret = () => Context_With_Same_DB_Invalidation();
                return ret;
            };

            var orgs = Context_With_Same_DB_Invalidation().Organization.Include("Departments").AsNoTracking().ToList();
            var deps = orgs.SelectMany(x => x.Departments).Count();
            Debug.WriteLine($"Unit of work: {orgs.Count} organizations with {deps} department");

            // loop body without caching
            Action<Func<EmployeeContext>> loobBody1 = getContext =>
            {
                using (var ctx = getContext())
                {
                    MyAccess da = new MyAccess(ctx);
                    foreach (var o in orgs)
                    {
                        da.GetOrganization_WithDepartments_AndCountryFlag(o.Id);
                    }
                }
            };

            // loop body with caching
            Action<Func<EmployeeContext>> loopBody2 = getContext =>
            {
                using (var ctx = getContext())
                {
                    MyAccess da = new MyAccess(ctx);
                    foreach (var o in orgs)
                    {
                        da.GetOrganization_WithDepartments_AndCountryFlag2(o.Id);
                    }
                }
            };

            var tests = new[]
            {
                new {Caption = "Without Caching", loop = loobBody1},
                new {Caption = "With Caching", loop = loopBody2},
            };

            foreach (var test in tests)
            {

                var ignored2 = StressIt.Run(init, test.loop, (ctx) => { }, 1, 111);
                Debug.WriteLine(test.Caption);
                foreach (int threads in new[] { 1, 4 })
                {
                    var report = StressIt.Run(
                        init,
                        test.loop,
                        (ctx) => { },
                        threads,
                        3000
                    );
                    Debug.WriteLine(" * " + threads + " threads. " + report.AsShort());
                }
            }


            Debug.WriteLine("Cache Requests: {0}. Hits: {1}",
                DependencyExtentions.TotalRequests,
                DependencyExtentions.TotalHits);


        }


        public EmployeeContext Context_Without_Invalidation()
        {
            return new EmployeeContext(
                DbEnv.CreateSqlConnection(_fileName),
                attachDefaultInvalidation: false);
        }

        public EmployeeContext Context_With_Same_DB_Invalidation()
        {
            return new EmployeeContext(
                DbEnv.CreateSqlConnection(_fileName),
                attachDefaultInvalidation: true);
        }

        public EmployeeContext Context_With_Redis_Invalidation()
        {
            var configOptions = new ConfigurationOptions
            {
                EndPoints = { "localhost" },
                AbortOnConnectFail = false,
            };

            RedisInvalidationStorage ri = new RedisInvalidationStorage(configOptions);
            var ret = new EmployeeContext(
                DbEnv.CreateSqlConnection(_fileName),
                attachDefaultInvalidation: false);

            CacheInvalidator invalidator = new CacheInvalidator(ret)
            {
                InvalidationStorage = ri,
                InvalidationStorageBuilder = ri
            };

            invalidator.AttachListener();
            ret.CacheInvalidator = invalidator;
            return ret;
        }


    }
}

