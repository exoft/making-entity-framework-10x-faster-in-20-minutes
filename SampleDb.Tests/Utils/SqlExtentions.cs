﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using Dapper;

namespace SampleDb.Tests.Utils
{
    public static class SqlExtentions
    {

        public static SqlConnection AsSqlConnection(this IDbConnection con)
        {
            if (con == null)
                throw new ArgumentNullException(nameof(con));

            if (con is SqlConnection)
                return (SqlConnection) con;

            throw new ArgumentException(
                $"SqlConnection instance is expected. Fact type is {con.GetType()}");
        }

        public static Version GetServerVersion(this SqlConnection con)
        {
            OpenIfClosed(con);
            int v = con.ExecuteScalar<int>("Select @@MICROSOFTVERSION");
            int v1 = v >> 24;
            int v2 = v >> 16 & 0xFF;
            int v3 = v & 0xFFFF;
            return new Version(v1, v2, v3);
        }

        public static string GetServerVersionLong(this SqlConnection con)
        {
            OpenIfClosed(con);
            var l = con.ExecuteScalar<string>("Select @@VERSION");
            l = l.Replace("\r", " ").Replace("\n", " ").Replace("\t", " ");
            while (l.IndexOf("  ") >= 0)
                l = l.Replace("  ", " ");

            return l;
        }

        public static T GetServerProperty<T>(this SqlConnection con, string propertyName)
        {
            OpenIfClosed(con);
            return con.ExecuteScalar<T>($"Select SERVERPROPERTY('{propertyName}')");
        }

        public static string GetServerEdition(this SqlConnection con)
        {
            return GetServerProperty<string>(con, "Edition");
        }

        public static EngineEdition GetServerEngineEdition(this SqlConnection con)
        {
            return (EngineEdition) GetServerProperty<int>(con, "EngineEdition");
        }

        public enum EngineEdition
        {
            [Description("Personal or Desktop Engine (Not available in SQL Server 2005 and later versions.)")]
            Personal = 1,

            [Description("Standard (This is returned for Standard, Web, and Business Intelligence.)")]
            Standard = 2,

            [Description("Enterprise (This is returned for Evaluation, Developer, and both Enterprise editions.)")]
            Enterprise = 3,

            [Description("Express (This is returned for Express, Express with Tools and Express with Advanced Services)")]
            Express = 4,

            [Description("SQL Database")]
            SQL_Database = 5,

            [Description("SQL Data Warehouse")]
            SQL_Data_Warehouse = 6,

        }


        public static SecurityMode GetServerSecurityMode(this SqlConnection con)
        {
            OpenIfClosed(con);
            return (SecurityMode) con.GetServerProperty<int>("IsIntegratedSecurityOnly");
        }


        public enum SecurityMode
        {
            [Description("Integrated security (Windows Authentication) only")]
            IntegratedOnly = 1,

            [Description("Both Windows Authentication and SQL Server Authentication")]
            Both = 0,
        }

        public static bool IsLocalDB(this SqlConnection con)
        {
            if (con.GetServerVersion().Major < 11)
                return false;

            return GetServerProperty<int>(con, "IsLocalDB") == 1;
        }

        public static FixedServerRoles IsInFixedRoles(this SqlConnection con)
        {
            OpenIfClosed(con);
            IEnumerable<FixedServerRoles> all = Enum.GetValues(typeof (FixedServerRoles)).OfType<FixedServerRoles>();
            FixedServerRoles ret = FixedServerRoles.None;
            foreach (var i in all)
            {
                int? @is = con.ExecuteScalar<int?>($"Select IS_SRVROLEMEMBER('{i}')");
                if (@is.HasValue && @is.Value != 0)
                    ret |= i;
            }

            return ret;
        }

        [Flags]
        public enum FixedServerRoles
        {
            None = 0,
            BulkAdmin = 128,
            BbCreator = 64,
            DiskAdmin = 32,
            ProcessAdmin = 16,
            SecurityAdmin = 8,
            ServerAdmin = 4,
            SetupAdmin = 2,
            SysAdmin = 1,
        }

        public static string GetServerProductLevel(this SqlConnection con)
        {
            return GetServerProperty<string>(con, "ProductLevel");
        }

        public static string GetDatabaseRecoveryMode(this SqlConnection con, string databaseName = null)
        {
            OpenIfClosed(con);
            if (databaseName == null)
                databaseName = GetCurrentDatabaseName(con);

            var sql = @"SELECT recovery_model_desc FROM sys.databases WHERE name = @name;";
            return con.ExecuteScalar<string>(sql, new {name = databaseName});
        }

        public static string GetCurrentDatabaseName(this SqlConnection con, int? id = null)
        {
            OpenIfClosed(con);
            if (id.HasValue)
                return (string)con.ExecuteScalar("Select DB_NAME(@id)", new { id = id.Value });
            else
                return (string)con.ExecuteScalar("Select DB_NAME()");
        }

        public static Dictionary<string, int> GetDatabases(this SqlConnection con)
        {
            var query = con.Query("exec sp_databases");
            Dictionary<string, int> ret = new Dictionary<string, int>();
            foreach (var o in query)
            {
                string name = o.DATABASE_NAME;
                int size = o.DATABASE_SIZE;
                ret[name] = size;
            }

            return ret;
        }

        public static int GetSPID(this SqlConnection con)
        {
            OpenIfClosed(con);
            return con.ExecuteScalar<int>("Select @@SPID");
        }

        public static List<int> ListConnections(this SqlConnection con, string databaseName)
        {
            OpenIfClosed(con);
            if (databaseName == null)
                databaseName = GetCurrentDatabaseName(con);

            var query = con.Query<SP_WhoItem>("exec sp_who");
            const StringComparison comp = StringComparison.InvariantCultureIgnoreCase;
            return query
                .Where(x => x.dbname != null && x.dbname.Equals(databaseName, comp))
                .Select(x => x.spid)
                .ToList();
        }

        class SP_WhoItem
        {
            public int spid;
            public string dbname;
        }

        public static void DropDatabase_And_CloseConnections(string connectionString, string databseName, bool throwOnError = true)
        {
            KillConnections(connectionString, databseName);
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                try
                {
                    con.Execute($"Drop Database [{databseName}]");
                }
                catch (Exception ex)
                {

                    if (throwOnError)
                        throw;
                    else
                        Debug.WriteLine(string.Format(
                            "Warning! Failed to drop database {0} on server {1}. {2}",
                            databseName, 
                            new SqlConnectionStringBuilder(connectionString).DataSource,
                            ex.GetExeptionDigest()
                            ));
                }
            }
        }

        public static void KillConnections(string connectionString, string databseName)
        {
            if (string.IsNullOrEmpty(connectionString))
                throw new ArgumentException("connectionString is null or empty", nameof(connectionString));

            if (string.IsNullOrEmpty(connectionString))
                throw new ArgumentException("databseName is null or empty", nameof(databseName));

            SqlConnectionStringBuilder b = new SqlConnectionStringBuilder(connectionString);
            b.Pooling = false;
            using (SqlConnection con = new SqlConnection(b.ConnectionString))
            {
                con.Open();
                var mySpid = con.GetSPID();
                var ids = con
                    .ListConnections(databseName)
                    .Where(id => id != mySpid);

                foreach (var id in ids)
                {
                    try
                    {
                        con.Execute($"Kill {id}");
                    }
                    catch(Exception ex)
                    {
                        Debug.WriteLine(
                            $"Warning! Failed to kill connection {id} to database {databseName}. {ex.GetExeptionDigest()}"
                            );
                    }
                }
            }
        }

        private static void OpenIfClosed(SqlConnection connection)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));

            if (connection.State == ConnectionState.Closed) connection.Open();
        }

        public static bool? GetAutoShrinkProperty(this SqlConnection con, string databaseName = null)
        {
            OpenIfClosed(con);
            if (databaseName == null)
                databaseName = GetCurrentDatabaseName(con);

            var sql = @"Select DatabasePropertyEx(@name, 'isautoshrink')";
            return con.ExecuteScalar<bool?>(sql, new { name = databaseName });
        }

        public static bool? SetAutoShrinkProperty(this SqlConnection con, string databaseName = null, bool needAutoShrink = false)
        {
            OpenIfClosed(con);
            if (databaseName == null)
                databaseName = GetCurrentDatabaseName(con);

            var sql = $"Alter Database [{databaseName}] SET AUTO_SHRINK " + (needAutoShrink ? "ON" : "OFF");
            return con.ExecuteScalar<bool?>(sql, new { name = databaseName });
        }

        public static void ShrinkDatabase(this SqlConnection con, string databaseName = null, ShrinkOptions options = ShrinkOptions.ShinkAndTruncate)
        {
            OpenIfClosed(con);
            if (databaseName == null)
                databaseName = GetCurrentDatabaseName(con);


            string so = "";
            if (options == ShrinkOptions.ShrinkOnly)
                so = ", NOTRUNCATE";
            else if (options == ShrinkOptions.TruncateOnly)
                so = ", TRUNCATEONLY";

            string sql = string.Format(
                @"DBCC SHRINKDATABASE ('{0}' {1}) WITH NO_INFOMSGS",
                databaseName, 
                so);

            con.Execute(sql);
        }

        public static long? GetDatabaseSize(this SqlConnection con, string databaseName = null, ShrinkOptions options = ShrinkOptions.ShinkAndTruncate)
        {
            OpenIfClosed(con);

            if (databaseName == null)
                databaseName = GetCurrentDatabaseName(con);

            int size;
            if (!GetDatabases(con).TryGetValue(databaseName, out size))
                return null;
            else
                return size;
        }



        public enum ShrinkOptions
        {
            ShinkAndTruncate,
            ShrinkOnly,
            TruncateOnly,
        }

    }
}