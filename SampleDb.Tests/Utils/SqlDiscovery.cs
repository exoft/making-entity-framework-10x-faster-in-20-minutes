﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDb.Tests.Utils
{
    using Microsoft.Win32;

    public static class SqlDiscovery
    {

        public static Dictionary<string, Version> GetLocalDbAndServerList()
        {
            Dictionary<string, Version> ret = new Dictionary<string, Version>(StringComparer.InvariantCultureIgnoreCase);
            foreach (var i in GetLocalDbList())
                ret[i.Key] = i.Value;

            foreach (var i in GetServerList())
                ret[i.Key] = i.Value;

            return ret;
        }

        public static Dictionary<string, Version> GetLocalDbList()
        {
            Dictionary<string, Version> ret = new Dictionary<string, Version>(StringComparer.InvariantCultureIgnoreCase);
            using (RegistryKey lm = Registry.LocalMachine)
            {
                // default instance
                using (RegistryKey k0 = lm.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SQL Server Local DB\Installed Versions", false))
                {
                    if (k0 != null)
                        foreach (var subKeyName in k0.GetSubKeyNames())
                        {
                            using (RegistryKey candidate = k0.OpenSubKey(subKeyName))
                                if (candidate != null)
                                {
                                    try
                                    {
                                        var versionStr = new Version(subKeyName);
                                        var instance = "(LocalDB)\\v" + subKeyName;
                                        ret[instance] = versionStr;
                                    }
                                    catch
                                    {
                                    }

                                }


                        }
                }
            }


            return ret;
        }

        public static Dictionary<string,Version> GetServerList()
        {
            Dictionary<string,Version> ret = new Dictionary<string, Version>(StringComparer.InvariantCultureIgnoreCase);
            using (RegistryKey lm = Registry.LocalMachine)
            {
                // default instance
                using (RegistryKey k0 = lm.OpenSubKey(@"SOFTWARE\Microsoft\MSSQLServer"))
                    if (k0 != null)
                        TryKey(k0, ret, string.Empty);

                // named instances
                using (RegistryKey k1 = lm.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SQL Server", false))
                    if (k1 != null)
                        foreach (string subKeyName in new List<string>(k1.GetSubKeyNames() ?? new string[0]))
                            using (RegistryKey candidate = k1.OpenSubKey(subKeyName))
                                if (candidate != null)
                                    TryKey(candidate, ret, subKeyName);
            }

            return ret;
        }

        private static void TryKey(RegistryKey k1, Dictionary<string, Version> ret, string instanceName)
        {
            string rawVersion = null;
            using (RegistryKey rk = k1.OpenSubKey(@"MSSQLServer\CurrentVersion", false))
                if (rk != null)
                    rawVersion = rk.GetValue("CurrentVersion") as string;

            string serviceKey = string.IsNullOrEmpty(instanceName) ? "MSSQLSERVER" : "MSSQL$" + instanceName;
            using (RegistryKey key = Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Services\" + serviceKey, false))
            {
                if (key == null)
                    return;
            }



            if (!string.IsNullOrEmpty(rawVersion) /* && rawPath != null && Directory.Exists(rawPath) */)
            {
                try
                {
                    Version ver = new Version(rawVersion);
                    string name = instanceName.Length == 0 ? "(local)" : ("(local)" + "\\" + instanceName);
                    ret[name] = ver;
                }
                catch
                {
                }
            }
        }

        public static IEnumerable<string> OrderByVersionDesc(this IDictionary<string, Version> list)
        {
            return list
                .OrderByDescending(x => x.Value == null ? new Version() : x.Value)
                .ThenByDescending(x => x.Key)
                .Select(x => x.Key);
        }

        public static string AsHumanReadableString(this IDictionary<string, Version> list)
        {
            var keys = OrderByVersionDesc(list);
            var strs = keys.Select(x => x + (list[x] == null ? "" : $" ({list[x]})"));
            return string.Join(", ", strs);
        }

    }
}
