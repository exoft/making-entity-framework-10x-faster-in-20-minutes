﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SampleDb.Tests
{
    class StressIt
    {
        public static StressResult Run<T>(
            Func<T> init,
            Action<T> loopBody,
            Action<T> dispose,
            int threads,
            int duration
        )
        {
            StressResult ret = new StressResult();
            List<Thread> list = new List<Thread>();
            CountdownEvent started = new CountdownEvent(threads);
            CountdownEvent finished = new CountdownEvent(threads);
            for (int i = 0; i < threads; i++)
            {
                int index = i;
                Thread t = new Thread(() =>
                {
                    T arg = default(T);
                    if (init != null) arg = init();
                    started.Signal();
                    started.Wait();
                    Stopwatch startedAt = Stopwatch.StartNew();
                    int total=0, failed=0, success=0;
                    while (startedAt.ElapsedMilliseconds < duration)
                    {
                        try
                        {
                            total++;
                            loopBody(arg);
                            success++;
                        }
                        catch (Exception ex)
                        {
                            failed++;
                            string ex2 = ex.ToString();
                            Debug.WriteLine(ex2);
                            Debugger.Break();
                        }
                    }

                    lock(ret)
                    ret.Threads.Add(new StressResult.ThreadInfo()
                    {
                        N = index,
                        Fail = failed,
                        Success = success,
                        Total = total,
                        Duration = startedAt.ElapsedMilliseconds
                    });

                    finished.Signal();


                }) {IsBackground = true};

                list.Add(t);
                t.Start();

            }

            finished.Wait();
            foreach (var thread in list)
            {
                thread.Join();
            }

            ret.Total = new StressResult.ThreadInfo()
            {
                N = ret.Threads.Count,
                Success = ret.Threads.Sum(info => info.Success),
                Total = ret.Threads.Sum(info => info.Total),
                Fail = ret.Threads.Sum(info => info.Fail),
                Duration = ret.Threads.Max(info => info.Duration)
            };

            return ret;
        }
    }

    public class StressResult
    {
        public List<ThreadInfo> Threads = new List<ThreadInfo>();

        public ThreadInfo Total = new ThreadInfo();
        public class ThreadInfo
        {
            public int N;
            public int Total;
            public int Success;
            public int Fail;
            public long Duration;
        }

        public string AsShort()
        {
            return $"{Total.Success} OK"
                + (Total.Fail > 0 ? ($" (+{Total.Fail} fails)") : "")
                + $" in {Total.Duration/1000m} secs";
        }
    }
}
